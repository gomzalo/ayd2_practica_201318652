const http = require('http');
const app = require('./app');


const { port } = require('./config')
const server = http.createServer(app);


//TEST
app.get('/', function (req, res) {
    res.status(200).send({
        message: `Practica AYD2, 201318652 corriendo en el puerto: ${port}.`
    });
});

app.listen(port, (err) => {
    if (err) console.log('Ocurrio un error'), process.exit(1);
    console.log(`Backend establecimiento corriendo en:\n\nhttp://localhost:${port}`);

});
