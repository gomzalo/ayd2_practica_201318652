const mysql = require('mysql2/promise')
var keys = require('./db_keys')


const pool = mysql.createPool(keys);


pool.getConnection(function(err, conn) {
    if (err) throw err;
    console.log("Conectado a BD.");
    pool.releaseConnection(conn);
})

module.exports = pool;