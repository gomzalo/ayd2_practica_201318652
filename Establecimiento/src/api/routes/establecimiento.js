const express = require('express');
const router = new express.Router();

const pool = require('../../config/database')
// :::::::::::::::::::::::::::::::::::::::
// :::::::::::::  ESTABLECIMIENTOS   :::::::::::::
// :::::::::::::::::::::::::::::::::::::::
// ___  Obtener todas los establecimientos ____
router.get('/getEstablecimiento', async function(req, res){
    const {codigo_establecimiento} =  req.body;
    var sql = 
    `SELECT * FROM Usr_Establecimiento
        WHERE Cod_Establecimiento = ${codigo_establecimiento};`;
    let getest = await pool.query(sql);
    let datos  = getest[0]; 
    // console.log(datos);
    if(datos.length > 0){
        res.status(200).json({
            data: datos
        })
    } else {
        res.status(200).json({
            data: "no establecimiento"
        })
    }
});
// ___  Obtener establecimiento ____
router.get('/getEstablecimientos', async function(req, res){

    var sql = `SELECT * FROM Usr_Establecimiento;`;
    let getest = await pool.query(sql);
    let datos  = getest[0]; 
    // console.log(datos);
    if(datos.length > 0){
        res.status(200).json({
            data: datos
        })
    } else {
        res.status(200).json({
            data: "no establecimientos"
        })
    }
});
// ___  Editar establecimiento ____
router.post('/editarEstablecimiento', async function(req, res){
    const {codigo_establecimiento, nombre, direccion, tipo, correo, contrasena, biografia, logo} =  req.body;

    var sql_nc = 
    `UPDATE Usr_Establecimiento SET
        Nombre = '${nombre}',
        Direccion = '${direccion}',
        Tipo_Establecimiento = '${tipo}',
        Correo = '${correo}',
        Contrasena = '${contrasena}',
        Biografia = '${biografia}',
        EscudoLogo = '${logo}'
    WHERE Cod_Establecimiento = ${codigo_establecimiento};`
    let nueva_clase = await pool.query(sql_nc);
    let n_class = JSON.stringify(nueva_clase[0]);
    let nc_affectedRows = JSON.parse(n_class).affectedRows
    console.log(nc_affectedRows);
    if(nc_affectedRows > 0){
        res.status(200).json({
            data: "establecimiento editado"
        });
    } else {
        res.status(200).json({
            data: "error"
        });
    }
});
// ___  Agregar imagenes establecimiento ____
router.post('/addImgEstablecimiento', async function(req, res){
    const {codigo_establecimiento, nombre, ruta, fecha} =  req.body;

    var sql_nc = 
    `INSERT INTO Imagen
    (Nombre, Ruta, Fecha_Publicacion, Cod_Establecimiento)
    VALUES
    ('${nombre}','${ruta}','${fecha}',${codigo_establecimiento});`
    let nueva_clase = await pool.query(sql_nc);
    let n_class = JSON.stringify(nueva_clase[0]);
    let nc_affectedRows = JSON.parse(n_class).affectedRows
    console.log(nc_affectedRows);
    if(nc_affectedRows > 0){
        res.status(200).json({
            data: "imagen cargada"
        });
    } else {
        res.status(200).json({
            data: "error"
        });
    }
});
// :::::::::::::::::::::::::::::::::::::::
// :::::::::::::  CLASES   :::::::::::::
// :::::::::::::::::::::::::::::::::::::::
// ___  Obtener todas las clases ____
router.get('/getClases', async function(req, res){
    var sql = `SELECT * FROM Clase;`;
    let getest = await pool.query(sql);
    let datos  = getest[0];
    // console.log(datos);
    if(datos.length > 0){
        res.status(200).json({
            data: datos
        });
    } else {
        res.status(200).json({
            data: "error"
        });
    }
});
// ___  Obtener clases pendientes ____
router.get('/getClases0', async function(req, res){
    const {codigo_establecimiento} =  req.body;
    var sql = 
    `SELECT Clase.Cod_Clase, Clase.Nombre, Clase.Descripcion, Clase.Seccion, Clase.Cod_Catedratico, Clase.Estado
    FROM Clase
    INNER JOIN Usr_Establecimiento ON Usr_Establecimiento.Cod_Establecimiento = Clase.Cod_Establecimiento
    WHERE Clase.Estado = '0'
    AND Usr_Establecimiento.Cod_Establecimiento = '${codigo_establecimiento}';`;
    let getest = await pool.query(sql);
    let datos  = getest[0];
    // console.log(datos);
    if(datos.length > 0){
        res.status(200).json({
            data: datos
        });
    } else {
        res.status(200).json({
            data: "no pendientes"
        });
    }
});
// ___  Obtener clases activas ____
router.get('/getClases1', async function(req, res){
    const {codigo_establecimiento} =  req.body;
    var sql =
    `SELECT Clase.Cod_Clase, Clase.Nombre, Clase.Descripcion, Clase.Seccion, Clase.Cod_Catedratico, Clase.Estado
    FROM Clase
    INNER JOIN Usr_Establecimiento ON Usr_Establecimiento.Cod_Establecimiento = Clase.Cod_Establecimiento
    WHERE Clase.Estado = '1'
    AND Usr_Establecimiento.Cod_Establecimiento = '${codigo_establecimiento}';`;
    let getest = await pool.query(sql);
    let datos  = getest[0];
    // console.log(datos);
    if(datos.length > 0){
        res.status(200).json({
            data: datos
        });
    } else {
        res.status(200).json({
            data: "no activas"
        });
    }
});
// ___  Nueva clase ____
router.post('/nuevaClase', async function(req, res){
    const {nombre_clase, descripcion_clase, seccion_clase, codigo_catedratico, codigo_establecimiento, estado_clase } =  req.body;
    // Verificando clase        
    var sql_vc = 
    `SELECT COUNT(*) as num FROM Clase
    WHERE Nombre = '${nombre_clase}'
    AND Seccion = '${seccion_clase}'
    AND Cod_Establecimiento = ${codigo_establecimiento};`;
    let verificar_clase = await pool.query(sql_vc);
    let ver_class = JSON.stringify(verificar_clase[0]);
    let vc_num = JSON.parse(ver_class)[0].num;

    console.log(vc_num)
    if(vc_num != 0){
        res.status(200).json({
            data: "clase existe"
        });
    } else {
        console.log("Clase no existe");
        var sql_nc = 
        `INSERT INTO Clase
        (Nombre, Descripcion, Seccion, Cod_Establecimiento, Cod_Catedratico, Estado)
        VALUES
        ('${nombre_clase}','${descripcion_clase}','${seccion_clase}',${codigo_establecimiento},${codigo_catedratico},'${estado_clase}');`
        let nueva_clase = await pool.query(sql_nc);
        let n_class = JSON.stringify(nueva_clase[0]);
        let nc_affectedRows = JSON.parse(n_class).affectedRows
        console.log(nc_affectedRows);
        if(nc_affectedRows > 0){
            res.status(200).json({
                data: "clase ingresada"
            });
        } else {
            res.status(200).json({
                data: "error"
            });
        }
    }
});
// :::::::::::::  REPORTE POR CLASE   :::::::::::::
// ___  Top clases con mas estudiantes registrados ____
router.get('/topmasestudiantes', async function(req, res){
    var sql =
    `SELECT Clase.Cod_Clase, Clase.Nombre, Clase.Descripcion, Clase.Seccion, Clase.Cod_Catedratico, Clase.Estado
    FROM Clase
    INNER JOIN Usr_Establecimiento ON Usr_Establecimiento.Cod_Establecimiento = Clase.Cod_Establecimiento
    WHERE Clase.Estado = '1'
    AND Usr_Establecimiento.Cod_Establecimiento = '1';`;
    let getest = await pool.query(sql);
    let datos  = getest[0];
    // console.log(datos);
    if(datos.length > 0){
        res.status(200).json({
            data: datos
        });
    } else {
        res.status(200).json({
            data: "no activas"
        });
    }
});
// ___  Top clases con menos estudiantes registrados ____
router.get('/topmenosestudiantes', async function(req, res){
    var sql =
    `SELECT Clase.Cod_Clase, Clase.Nombre, Clase.Descripcion, Clase.Seccion, Clase.Cod_Catedratico, Clase.Estado
    FROM Clase
    INNER JOIN Usr_Establecimiento ON Usr_Establecimiento.Cod_Establecimiento = Clase.Cod_Establecimiento
    WHERE Clase.Estado = '1'
    AND Usr_Establecimiento.Cod_Establecimiento = '1';`;
    let getest = await pool.query(sql);
    let datos  = getest[0];
    // console.log(datos);
    if(datos.length > 0){
        res.status(200).json({
            data: datos
        });
    } else {
        res.status(200).json({
            data: "no activas"
        });
    }
});
// ___  % de los estudiantes registrados en cada clase ____
router.get('/estudiantesregistradosclase', async function(req, res){
    var sql =
    `SELECT Clase.Cod_Clase, Clase.Nombre, Clase.Descripcion, Clase.Seccion, Clase.Cod_Catedratico, Clase.Estado
    FROM Clase
    INNER JOIN Usr_Establecimiento ON Usr_Establecimiento.Cod_Establecimiento = Clase.Cod_Establecimiento
    WHERE Clase.Estado = '1'
    AND Usr_Establecimiento.Cod_Establecimiento = '1';`;
    let getest = await pool.query(sql);
    let datos  = getest[0];
    // console.log(datos);
    if(datos.length > 0){
        res.status(200).json({
            data: datos
        });
    } else {
        res.status(200).json({
            data: "no activas"
        });
    }
});

module.exports = router