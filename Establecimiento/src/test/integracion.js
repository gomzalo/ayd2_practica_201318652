const chai = require('chai');
const expect = chai.expect;
const request = chai.request;
const chaiHttp = require('chai-http');
const app = require('../app');
chai.use(chaiHttp);

const url = app;

describe('Establecimiento', () => {
    it('GET todos los establecimientos', (done) => {
        chai.request(url)
            .get("/getestablecimientos")
            .end(function (err, res) {
                console.log("Test integracion 201318652")
                expect(res).to.have.status(200);
                done();
            });
    });

    it('GET todas las clases', (done) => {
        chai.request(url)
            .get("/getclases")
            .end(function (err, res) {
                // console.log(res.body)
                console.log("Test integracion 201318652")
                expect(res).to.have.status(200);
                done();
            });
    });

});