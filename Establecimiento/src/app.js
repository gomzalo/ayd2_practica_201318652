const express = require('express')
const app = express()
// var methodOverride = require("method-override")
const path = require("path")
const con = require("./config/database.js")
const cors = require('cors')
const bodyParser = require('body-parser')
const config = require('./config/init')

// connecting route to database
// app.use(function(req, res, next) {
//     req.con = con
//     next()
//   })



// parsing body request
app.use(bodyParser.urlencoded({ extended: false}))
app.use(bodyParser.json())
app.use(config.cors)
// app.use(methodOverride("_method"))

//  Rutas
const establecimientosRoutes = require('./api/routes/establecimiento')
// Routing
app.use('/', establecimientosRoutes)

module.exports = app